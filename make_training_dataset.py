import os
import glob
import json

import numpy as np

DATA_ROOT = 'results/TLGGICCUP_gosu_features'
SAVE_ROOT = 'results/TLGGICCUP_gosu_training_dataset_C'
SUB_FOLDER = 'PvZ'
SPECIES = 'Protoss'
DURATION = 1000

SAVE_FOLDER = os.path.join(SAVE_ROOT, SUB_FOLDER, SPECIES)
if not os.path.isdir(SAVE_FOLDER):
    os.makedirs(SAVE_FOLDER)
rgds = sorted(glob.glob(os.path.join(DATA_ROOT, SUB_FOLDER, SPECIES, '*.rgd')))

for i, rgd in enumerate(rgds):
    print('processing {}/{}'.format(i+1, len(rgds)))
    print('\t'+rgd)

    with open(rgd) as f:
        features = json.load(f)

    dataset = []
    last_frame_id, last_feature = 0, None
    for frame_id, feature, action in features:
        if action > 0 and frame_id != 0 or frame_id - last_frame_id >= DURATION:
            dataset.append(last_feature + [action])
            last_frame_id = frame_id

        last_feature = feature

    np.savetxt(os.path.join(SAVE_FOLDER, os.path.basename(rgd)), dataset, delimiter=',', fmt='%d')
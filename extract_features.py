import os
import glob
import json
from collections import OrderedDict

DATA_ROOT = 'results/TLGGICCUP_gosu_building_order'
SAVE_ROOT = 'results/TLGGICCUP_gosu_features'
SUB_FOLDER = 'PvZ'
SPECIES = 'Protoss'

SAVE_FOLDER = os.path.join(SAVE_ROOT, SUB_FOLDER, SPECIES)
if not os.path.isdir(SAVE_FOLDER):
    os.makedirs(SAVE_FOLDER)
FEATURE_KEY_FOLDER = os.path.join(SAVE_ROOT, 'features_actions_definition', SUB_FOLDER)
if not os.path.isdir(FEATURE_KEY_FOLDER):
    os.makedirs(FEATURE_KEY_FOLDER)
rgds = sorted(glob.glob(os.path.join(DATA_ROOT, SUB_FOLDER, '*.rgd')))

######################### Extract Features #############################
def extract_features():
    with open(os.path.join(FEATURE_KEY_FOLDER, SPECIES)) as f:
        definition = json.load(f)
    feature_keys, actions = definition['feature_keys'], definition['actions']

    for i, rgd in enumerate(rgds):
        print('processing {}/{}'.format(i+1, len(rgds)))
        print('\t'+rgd)

        with open(rgd) as f:
            states = json.load(f)
        species_states = states[SPECIES]

        features = []
        for state in species_states:
            feature = OrderedDict([(key, 0) for key in feature_keys])
            for k, v in state.items():
                if k in feature:
                    feature[k] = int(v)
            feature = list(feature.values())
            action = actions[state['commands'][2]] if state['commands'][0] == 'Created' else 0
            features.append((state['frame_id'], feature, action))
            
        with open(os.path.join(SAVE_FOLDER, os.path.basename(rgd)), 'w') as f:
            f.write(json.dumps(features))

################# Extract Features Keys ############################
def extract_feature_keys_actions():
    feature_keys, actions = set(), set()
    for i, rgd in enumerate(rgds):
        print('processing {}/{}'.format(i+1, len(rgds)))
        print('\t'+rgd)

        with open(rgd) as f:
            states = json.load(f)
        species_states = states[SPECIES]
        for state in species_states:
            feature_keys.update(state.keys())
            if state['commands'][0] == 'Created':
                actions.add(state['commands'][2])

    definition = {}

    feature_keys.remove('commands')
    feature_keys.remove('frame_id')
    definition['feature_keys'] = sorted(list(feature_keys))

    actions = {action: idx+1 for idx, action in enumerate(sorted(list(actions)))}
    actions['None'] = 0
    definition['actions'] = actions
    with open(os.path.join(FEATURE_KEY_FOLDER, SPECIES), 'w') as f:
        f.write(json.dumps(definition))

extract_features()
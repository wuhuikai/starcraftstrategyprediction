import os
import h5py
import numpy as np

TYPE = 'n_to_1'
DATA_ROOT = 'results/training_dataset'
DATA_TYPE = 'C'
SUB_FOLDER = 'PvZ'
SPECIES = 'Protoss'

DATA_FOLDER = os.path.join(TYPE, DATA_ROOT, DATA_TYPE, SUB_FOLDER, SPECIES)

for name in ['train', 'val', 'test']:
    data = np.genfromtxt(os.path.join(DATA_FOLDER, name+'.csv'), delimiter=',', dtype=int)

    X = data[:, :-1].astype(np.float32)
    y = data[:, -1].astype(np.int32)

    mean = np.mean(X, 0)
    std = np.std(X, 0)

    f = h5py.File(os.path.join(DATA_FOLDER, name+'.hdf5'), 'w')
    f.create_dataset('X', data=X)
    f.create_dataset('y', data=y)
    f.create_dataset('mean', data=mean)
    f.create_dataset('std', data=std)
    f.close()
import sys
import h5py

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data

from torch.autograd import Variable

from torchsample.modules import ModuleTrainer
from torchsample.metrics import CategoricalAccuracy
from torchsample.callbacks import *
from torchsample.initializers import *

from yellowfin import YFOptimizer
"""
    python train_nn.py [true|false]
"""

DATA_ROOT = 'results/training_dataset'
SAVE_ROOT = 'results/models'
TYPE = 'n_to_1'
DATA_TYPE = 'C'
SUB_FOLDER = 'PvZ'
SPECIES = 'Protoss'
SEEK = sys.argv[1] == 'true'

DATA_FOLDER = os.path.join('../..', TYPE, DATA_ROOT, DATA_TYPE, SUB_FOLDER, SPECIES)

SAVE_FOLDER = os.path.join('../..', TYPE, SAVE_ROOT, DATA_TYPE, SUB_FOLDER, SPECIES)
if not os.path.isdir(SAVE_FOLDER):
    os.makedirs(SAVE_FOLDER)

#################################################################################################

class HDF5LoaderIter(object):
    def __init__(self, data_loader):
        self.dataset = data_loader.dataset
        
        self.batch_size = data_loader.batch_size
        self.idx = 0
        
    def __len__(self):
        return len(self.dataset) // self.batch_size
        
    def __next__(self):
        if self.idx > len(self.dataset):
            raise StopIteration

        results = self.dataset[self.idx:self.idx+self.batch_size]
        self.idx += self.batch_size

        idx = torch.randperm(results[0].shape[0])
        return [data.dataloader.default_collate(r)[idx] for r in results]

    next = __next__  # Python 2 compatibility

    def __iter__(self):
        return self

class HDF5Dataset(data.Dataset):
    def __init__(self, path):
        data = h5py.File(path, 'r')
        self.X = data['X']
        self.y = data['y']
        self.mean = np.asarray(data['mean'])
        self.std = np.asarray(data['std']) + 1e-5

    def __getitem__(self, index):
        X = (self.X[index] - self.mean)/self.std
        X = np.reshape(X[:, :-9], (-1, 10, 64))

        return X, self.y[index].astype(int)

    def __len__(self):
        if SEEK:
            return 43194
        return self.y.shape[0]

class HDF5DataLoader(object):
    def __init__(self, path, batch_size=1):
        self.dataset = HDF5Dataset(path)
        self.batch_size = batch_size
        
    def __iter__(self):
        return HDF5LoaderIter(self)

    def __len__(self):
        return len(self.dataset) // self.batch_size

#################################################################################################

train_loader = HDF5DataLoader(os.path.join(DATA_FOLDER, 'train.hdf5'), 128)
val_loader = HDF5DataLoader(os.path.join(DATA_FOLDER, 'val.hdf5'), 128)
val_loader.dataset.mean = train_loader.dataset.mean
val_loader.dataset.std = train_loader.dataset.std

#################################################################################################

class SimpleRNN(nn.Module):
    def __init__(self, drop_prob=0.2):
        super(SimpleRNN, self).__init__()
        self.linear1 = nn.Linear(64, 128)

        self.rnn1 = nn.GRUCell(input_size=128, hidden_size=256)
        self.drop = nn.Dropout(drop_prob)
        self.rnn2 = nn.GRUCell(input_size=256, hidden_size=256)

        self.linear2 = nn.Linear(256, 31)

    def forward(self, X):
        X = X.transpose(0, 1)

        h1 = Variable(X.data.new().resize_((X.size(1), 256)).zero_())
        h2 = Variable(X.data.new().resize_((X.size(1), 256)).zero_())

        for x in X:
            x = F.relu(self.linear1(x))
            h1 = self.rnn1(x, h1)
            x = self.drop(h1)
            h2 = self.rnn2(x, h2)

        x = self.linear2(F.relu(h2))
        return x

#################################################################################################

def make_model(_model_params):
    model_params = {'drop_prob': 0.5}  # 53.11%
    model_params.update(_model_params)
    return SimpleRNN(**model_params), {'optimizer_lr': 0.001, 'optimizer_weight_decay': 0.0001}, 'RNN'

##################################################################################################

class Evaluate(Callback):
    def __init__(self, data_loader):
        self.max_acc = None
        self.data_loader = data_loader
        super(Evaluate, self).__init__()

    def on_epoch_end(self, epoch, logs=None):
        correct = 0
        self.model.model.eval()
        for X, y in iter(self.data_loader):
            X = Variable(X.cuda(), volatile=True)
            p_y = np.argmax(self.model.model(X).data.cpu().numpy(), axis=1)
            correct += np.sum(p_y == y.numpy())
        self.model.model.train()

        acc = correct / len(self.data_loader.dataset)
        if self.max_acc is None or self.max_acc < acc:
            self.max_acc = acc
        # else:
        #     self.model._optimizer.set_lr_factor(self.model._optimizer.get_lr_factor()/4.0)

        print('### acc[val]: {}%, acc[max val]: {}%'.format(acc*100, self.max_acc*100))

##################################################################################################

def train(nb_epoch=200, _params={}, _model_params={}):
    model, params, name = make_model(_model_params)
    params.update(_params)

    trainer = ModuleTrainer(model.cuda())
    evaluator = Evaluate(val_loader)
    initializers = [Uniform(-0.1, 0.1, module_filter='linear*'), Orthogonal(module_filter='rnn*')]
    trainer.compile(loss='cross_entropy', optimizer='Adam', metrics=CategoricalAccuracy(),
        initializers=initializers,
        callbacks=[
            CSVLogger('results/{}_{}_acc.csv'.format(TYPE, name)),
            ModelCheckpoint(SAVE_FOLDER, name+'_{epoch}_{loss}.pth', max_save=5, save_best_only=True),
            ReduceLROnPlateau(),
            # LearningRateScheduler(lambda epoch, lrs, **kwargs: [lr * 0.9 ** epoch for lr in lrs]),
            evaluator
        ], **params)

    trainer.fit_loader(train_loader, val_loader=val_loader, nb_epoch=nb_epoch, verbose=1, cuda_device=0)
    return evaluator.max_acc
##################################################################################################

def params_seeker(n_iter=100, nb_epoch=200):

    max_acc, max_params, max_model_params = None, None, None
    for idx in range(n_iter):
        print('{} [{}|{}] {}'.format('*'*40, idx+1, n_iter, '*'*40))

        lr = 10 ** np.random.uniform(-4, -1)
        weight_decay = 10 ** np.random.uniform(-5, 0)
        drop_prob = np.random.uniform(0, 1)
        _params = {'optimizer_lr': lr, 'optimizer_weight_decay': weight_decay}
        _model_params = {'drop_prob': drop_prob}

        print('PARAMS: {}\nMODEL PARAMS: {}\nTraining start ...\n'.format(_params, _model_params))
        acc = train(nb_epoch=nb_epoch, _params=_params, _model_params=_model_params)
        if max_acc is None or acc > max_acc:
            max_acc, max_params, max_model_params = acc, _params, _model_params
        print('\nACC[params={}, model params={}]: {} MAX_ACC[params={}, model params={}]: {}'.format(_params, \
                _model_params, acc, max_params, max_model_params, max_acc))

##################################################################################################

"""
    C:
        n --> 1
            RNN: 53.11%
"""
t = time.time()
#######################################################################
if SEEK:
    params_seeker()
else:
    train()
#######################################################################
dur = time.time() - t
print('Totoal time: {} s'.format(dur))
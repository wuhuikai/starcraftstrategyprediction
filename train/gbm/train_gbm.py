import os
import sys
import time
import h5py
import pickle

"""
    python train_gbm.py [1to1|n_to_1] [x|l]
"""

TYPE = sys.argv[1]
DATA_ROOT = 'results/training_dataset'
SAVE_ROOT = 'results/models'
DATA_TYPE = 'C'
SUB_FOLDER = 'PvZ'
SPECIES = 'Protoss'

DATA_FOLDER = os.path.join('../..', TYPE, DATA_ROOT, DATA_TYPE, SUB_FOLDER, SPECIES)

SAVE_FOLDER = os.path.join('../..', TYPE, SAVE_ROOT, DATA_TYPE, SUB_FOLDER, SPECIES)
if not os.path.isdir(SAVE_FOLDER):
    os.makedirs(SAVE_FOLDER)

train = h5py.File(os.path.join(DATA_FOLDER, 'train.hdf5'), 'r')
val = h5py.File(os.path.join(DATA_FOLDER, 'val.hdf5'), 'r')

train_X, train_y = train['X'], train['y']
val_X, val_y = val['X'], val['y']

#################################################################################################

def xgboost_train():
    import xgboost as xgb
    clf = xgb.XGBClassifier().fit(train_X, train_y)
    print(clf.score(val_X, val_y))
    with open(os.path.join(SAVE_FOLDER, 'xgboost.pkl'), 'wb') as f:
        pickle.dump(clf, f)

#################################################################################################

def lightgbm_train():
    import lightgbm as lgb
    clf = lgb.LGBMClassifier(objective='multiclass').fit(train_X, train_y)

    print(clf.score(val_X, val_y))
    with open(os.path.join(SAVE_FOLDER, 'lightgbm.pkl'), 'wb') as f:
        pickle.dump(clf, f)

##################################################################################################

"""
    C:
        1 --> 1
          RandomForest: 36.75% Lightgbm: 44.00%  xgBoost: 43.65%
        n --> 1
                               Lightgbm: 52.39%  xgBoost: 51.44%
"""
t = time.time()
if sys.argv[2] == 'l':
    lightgbm_train()
elif sys.argv[2] == 'x':
    xgboost_train()
else:
    pass
dur = time.time() - t
print('Totoal time: {} s'.format(dur))
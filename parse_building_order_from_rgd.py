import os
import glob
import copy
import json

DATA_ROOT = '/Users/wuhuikai/Downloads/TLGGICCUP_gosu_data'
SAVE_ROOT = 'results/TLGGICCUP_gosu_building_order'
COMMANDS = set(['Created', 'Destroyed', 'Discovered', 'Morph', 'StartResearch', 'FinishResearch', 'CancelResearch', 'StartUpgrade', 'FinishUpgrade', 'CancelUpgrade'])
SUB_FOLDER = 'PvZ'

SAVE_FOLDER = os.path.join(SAVE_ROOT, SUB_FOLDER)
if not os.path.isdir(SAVE_FOLDER):
    os.makedirs(SAVE_FOLDER)

rgds = sorted(glob.glob(os.path.join(DATA_ROOT, SUB_FOLDER, '*.rgd')))

for i, rgd in enumerate(rgds):
    print('processing {}/{}'.format(i+1, len(rgds)))
    print('\t'+rgd)

    errors = []
    with open(rgd, encoding='windows-1252') as f:
        # Skip first 5 lines
        while True:
            if 'The following players are in this replay:' in next(f):
                break
        # Which id
        count = 0
        for line in f:
            if 'Begin replay data:' in line:
                break
            words = [w.strip() for w in line.split(',')]
            if count == 0:
                species_a_id, SPECIES_A = words[0], words[2]
            if count == 1:
                species_b_id, SPECIES_B = words[0], words[2]
            count += 1
        if count > 2:
            continue
        # Game Begin
        states = {SPECIES_A:[{}], SPECIES_B:[{}], SPECIES_A+'_PO':[{}], SPECIES_B+'_PO':[{}]}
        for line in f:
            if '[EndGame]' in line:
                break

            words = [w.strip() for w in line.split(',')]
            if len(words) < 3:
                continue
            frame_id, idx, command = words[:3]
            # '0' or '1'
            if idx != species_a_id and idx != species_b_id:
                continue

            # Which set
            if idx == species_a_id:
                state_key = SPECIES_A
            if idx == species_b_id:
                state_key = SPECIES_B
            SPECIES = state_key
            if command == 'Discovered':
                state_key += '_PO'

            state = states[state_key][-1]
            # which commands
            state['frame_id'] = int(frame_id)
            if command == 'Created':
                unit_id, unit_type = words[3], words[4]
                if SPECIES != unit_type.split(' ')[0].strip():
                    errors.append('{}, {}, {}, {}, {}'.format(line.strip(), SPECIES_A, species_a_id, SPECIES_B, species_b_id))
                    continue

                if unit_type in state:
                    state[unit_type].add(unit_id)
                else:
                    state[unit_type] = set([unit_id])
                state['commands'] = [command, unit_id, unit_type]
            elif command == 'Destroyed':
                unit_id, unit_type = words[3], words[4]
                if SPECIES != unit_type.split(' ')[0].strip():
                    errors.append('{}, {}, {}, {}, {}'.format(line.strip(), SPECIES_A, species_a_id, SPECIES_B, species_b_id))
                    continue

                if unit_type in state and unit_id in state[unit_type]:
                    state[unit_type].remove(unit_id)
                state['commands'] = [command, unit_id, unit_type]
            elif command == 'Discovered':
                unit_id, unit_type = words[3], words[4]
                if SPECIES != unit_type.split(' ')[0].strip():
                    errors.append('{}, {}, {}, {}, {}'.format(line.strip(), SPECIES_A, species_a_id, SPECIES_B, species_b_id))
                    continue

                if unit_type in state:
                    state[unit_type].add(unit_id)
                else:
                    state[unit_type] = set([unit_id])
                state['commands'] = [command, unit_id, unit_type]
            elif command == 'Morph':
                unit_id, unit_type = words[3], words[4]
                if SPECIES != unit_type.split(' ')[0].strip():
                    errors.append('{}, {}, {}, {}, {}'.format(line.strip(), SPECIES_A, species_a_id, SPECIES_B, species_b_id))
                    continue

                if unit_type in state:
                    state[unit_type].add(unit_id)
                else:
                    state[unit_type] = set([unit_id])

                for v in state.values():
                    if isinstance(v, set) and unit_id in v:
                        v.remove(unit_id)
                state['commands'] = [command, unit_id, unit_type]
            elif command == 'FinishResearch':
                unit_type = words[3]
                state[unit_type] = True
                state['commands'] = [command, unit_type]
            elif command == 'FinishUpgrade':
                unit_type, level = words[3], words[4]
                state[unit_type] = int(level)
                state['commands'] = [command, unit_type, level]
            elif command == 'R':
                minerals, gas, gatheredMinerals, gatheredGas, supplyUsed, supplyTotal = words[3:]
                state['minerals'] = minerals
                state['gas'] = gas
                state['gatheredMinerals'] = gatheredMinerals
                state['gatheredGas'] = gatheredGas
                state['supplyUsed'] = supplyUsed
                state['supplyTotal'] = supplyTotal
                state['commands'] = [command, minerals, gas, gatheredMinerals, gatheredGas, supplyUsed, supplyTotal]
            else:
                continue

            # New State
            states[state_key].append(copy.deepcopy(state))
            for k, v in state.items():
                if isinstance(v, set):
                    state[k] = len(v)

    for k in states:
        states[k] = states[k][:-1]
    with open(os.path.join(SAVE_FOLDER, os.path.basename(rgd)), 'w') as f:
        # f.write(pprint.pformat(states))
        f.write(json.dumps(states))
    
    if len(errors) > 0:
        with open(os.path.join(SAVE_ROOT, SUB_FOLDER+'.txt'), 'a+') as f:
            f.write(rgd+'\n\t')
            f.write('\n\t'.join(errors)+'\n')
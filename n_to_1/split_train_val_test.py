import os
import glob

import numpy as np

DATA_ROOT = '../results/TLGGICCUP_gosu_training_dataset'
SAVE_ROOT = 'results/training_dataset'
TYPE = 'C'
SUB_FOLDER = 'PvZ'
SPECIES = 'Protoss'
N = 10
TRAIN, VAL, TEST = 0.7, 0.1, 0.2

SAVE_FOLDER = os.path.join(SAVE_ROOT, TYPE, SUB_FOLDER, SPECIES)
if not os.path.isdir(SAVE_FOLDER):
    os.makedirs(SAVE_FOLDER)
rgds = sorted(glob.glob(os.path.join(DATA_ROOT+'_'+TYPE, SUB_FOLDER, SPECIES, '*.rgd')))

features = []
for i, rgd in enumerate(rgds):
    print('processing {}/{}'.format(i+1, len(rgds)))
    print('\t'+rgd)

    raw_featurtes = np.genfromtxt(rgd, delimiter=',', dtype=int)
    for s_i in range(raw_featurtes.shape[0]-N):
        X, y = [], []
        for idx in range(N):
            X.append(raw_featurtes[s_i+idx][:-1])
            y.append(raw_featurtes[s_i+idx][-1])
        X.append(np.asarray(y))
        features.append(np.hstack(X))
    
features = np.vstack(features)
lenth = features.shape[0]
idx = np.arange(lenth)
np.random.shuffle(idx)
train_end, val_end = int(lenth*TRAIN), int(lenth*(TRAIN+VAL))
"""
    total: 431942, train: 302359, val: 43194, test: 86389
"""
print(lenth, train_end, val_end-train_end, lenth-val_end)

train_features, val_features, test_features = features[:train_end], features[train_end:val_end], features[val_end:]
np.savetxt(os.path.join(SAVE_FOLDER, 'train.csv'), train_features, delimiter=',', fmt='%d')
np.savetxt(os.path.join(SAVE_FOLDER, 'val.csv'), val_features, delimiter=',', fmt='%d')
np.savetxt(os.path.join(SAVE_FOLDER, 'test.csv'), test_features, delimiter=',', fmt='%d')
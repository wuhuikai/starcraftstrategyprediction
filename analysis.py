import os
import glob
import json

import numpy as np

import visdom

DATA_ROOT = 'results/TLGGICCUP_gosu_features'
SUB_FOLDER = 'PvZ'
SPECIES = 'Protoss'

rgds = sorted(glob.glob(os.path.join(DATA_ROOT, SUB_FOLDER, SPECIES, '*.rgd')))
vis = visdom.Visdom(server='http://159.226.178.126', port=8097, env='starcraft')

length = []
created_durations = []
created_durations_stat = []
for i, rgd in enumerate(rgds):
    print('processing {}/{}'.format(i+1, len(rgds)))
    print('\t'+rgd)

    with open(rgd) as f:
        features = json.load(f)

    created_frame_ids = []
    for frame_id, _, action in features:
        if action != 0 and frame_id > 0:
            created_frame_ids.append(frame_id)

    diff = [j-i for i, j in zip(created_frame_ids[:-1], created_frame_ids[1:])]
    if diff == 0:
        continue
    length.append(len(diff))
    created_durations += diff
    created_durations_stat.append((np.mean(diff), np.median(diff)))

vis.line(Y=np.asarray(created_durations_stat), win=0, opts={'legend': ['mean', 'median']})
"""
Length:
    max: 980, min:0, avg: 240.977, median: 207.0
Duration:
    max: 46777, min:0, avg: 145.895, median: 65.0
"""
print(np.max(length), np.min(length), np.mean(length), np.median(length))
print(np.max(created_durations), np.min(created_durations), np.mean(created_durations), np.median(created_durations))
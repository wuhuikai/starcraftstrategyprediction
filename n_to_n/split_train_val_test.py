import os
import glob
import h5py

import numpy as np

DATA_ROOT = '../results/TLGGICCUP_gosu_training_dataset'
SAVE_ROOT = 'results/training_dataset'
TYPE = 'C'
SUB_FOLDER = 'PvZ'
SPECIES = 'Protoss'
TRAIN, VAL, TEST = 0.7, 0.1, 0.2

SAVE_FOLDER = os.path.join(SAVE_ROOT, TYPE, SUB_FOLDER, SPECIES)
if not os.path.isdir(SAVE_FOLDER):
    os.makedirs(SAVE_FOLDER)
rgds = sorted(glob.glob(os.path.join(DATA_ROOT+'_'+TYPE, SUB_FOLDER, SPECIES, '*.rgd')))
np.random.shuffle(rgds)

lenth = len(rgds)
train_end, val_end = int(lenth*TRAIN), int(lenth*(TRAIN+VAL))

def make_data(rgds, name):
    features, ys = [], []
    print(name)
    for i, rgd in enumerate(rgds):
        print('\tprocessing {}/{}'.format(i+1, len(rgds)))
        print('\t\t'+rgd)

        raw_featurtes = np.genfromtxt(rgd, delimiter=',', dtype=int)
        for f in raw_featurtes:
            features.append(f[:-1])
            ys.append(f[-1])

        features.append(np.zeros(features[-1].shape, dtype=np.float32))
        ys.append(-1)
        
    features = np.vstack(features)
    ys = np.vstack(ys)

    f = h5py.File(os.path.join(SAVE_FOLDER, name+'.hdf5'), 'w')
    f.create_dataset('X', data=features)
    f.create_dataset('y', data=ys)
    f.close()

make_data(rgds[:train_end], 'train')
make_data(rgds[train_end:val_end], 'val')
make_data(rgds[val_end:], 'test')